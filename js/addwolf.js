const saveWolf = document.querySelector(".button button");
const myHeaders = {
    "Content-Type" : "application/json"
}


function post(name,age,link,description) {

    const newWolf = {
        "wolf":{
            "photo" : link,
            "age" : age,
            "name" : name,
            "description" : description
            }
        }
    
    
    const fetchConfig = {
        method:'POST',
        headers: myHeaders,
        body: JSON.stringify(newWolf)
    }

    fetch('https://lobinhos-api.herokuapp.com/wolves',fetchConfig)
    .then(pareseRequest => pareseRequest.json())
    .catch(reject => {
        alert(reject);
    });
}

saveWolf.addEventListener("click",(e)=>{
    e.preventDefault();
    const inputFormName = document.querySelector(".wolfsname input");
    const inputFormAge = document.querySelector(".wolfsage input");
    const inputFormLink = document.querySelector(".wolfslink input");
    const inputFormDescription = document.querySelector(".wolfsdescription textarea");

    let name = inputFormName.value;
    let age =inputFormAge.value;
    let link = inputFormLink.value;
    let description = inputFormDescription.value;

    post(name,age,link,description);
});