window.onscroll = function() {headerFixed()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function headerFixed() {
    if (window.pageYOffset > sticky) {
    header.style.height = '100px';
    header.classList.add("sticky");
    } else {
    header.classList.remove("sticky");
    header.style.height = '130px';
    }
}