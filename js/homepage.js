const LeftDiv = document.querySelector("#wolfsLeft");
const textDivLeft = document.querySelector("#textsLeft"); 

const RightDiv = document.querySelector("#wolfsRight");
const textDivRigth = document.querySelector("#textsRight");
 
function getL() {
    fetch('https://lobinhos-api.herokuapp.com/wolves') 
    .then(parseRequest => parseRequest.json())
    .then(responseAsJson => {
        let imageWolf = document.createElement("img");
        let nameWolf = document.createElement("h2");
        let ageWolf = document.createElement("p");
        let descriptionWolf = document.createElement("p");
        
        imageWolf.src = responseAsJson[0].photo;
        nameWolf.innerText = responseAsJson[0].name;
        ageWolf.innerText = `Idade: ${responseAsJson[0].age} anos`;
        descriptionWolf.innerText = responseAsJson[0].description; 

        imageWolf.classList.add("wolfs-imgL");
        nameWolf.classList.add("wolfs-nameL");
        ageWolf.classList.add("wolfs-ageL");
        descriptionWolf.classList.add("wolfs-descriptionL");

        LeftDiv.appendChild(imageWolf);
        textDivLeft.appendChild(nameWolf);
        textDivLeft.appendChild(ageWolf);
        textDivLeft.appendChild(descriptionWolf);
        LeftDiv.appendChild(textDivLeft);

    })  
    .catch(reject => {
        alert(reject);
    })
}

function getR() {
    fetch('https://lobinhos-api.herokuapp.com/wolves') 
    .then(parseRequest => parseRequest.json())
    .then(responseAsJson => {
        let imageWolfR = document.createElement("img");
        let nameWolfR = document.createElement("h2");
        let ageWolfR = document.createElement("p");
        let descriptionWolfR = document.createElement("p");
        
        imageWolfR.src = responseAsJson[1].photo;
        nameWolfR.innerText = responseAsJson[1].name;
        ageWolfR.innerText = `Idade: ${responseAsJson[1].age} anos`;
        descriptionWolfR.innerText = responseAsJson[1].description; 

        imageWolfR.classList.add("wolfs-imgR");
        nameWolfR.classList.add("wolfs-nameR");
        ageWolfR.classList.add("wolfs-ageR");
        descriptionWolfR.classList.add("wolfs-descriptionR");

        RightDiv.appendChild(imageWolfR);
        textDivRigth.appendChild(nameWolfR);
        textDivRigth.appendChild(ageWolfR);
        textDivRigth.appendChild(descriptionWolfR);
        RightDiv.appendChild(textDivRigth);

    })  
    .catch(reject => {
        alert(reject);
    })
}


getL();
getR();